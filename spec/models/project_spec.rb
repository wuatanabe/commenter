require 'spec_helper'

describe Project do
    let!(:project) { FactoryGirl.create(:project) }   
    let!(:comment) { FactoryGirl.create(:comment, project: project) }  
    
  describe "creation" do

	  context  "valid attributes" do
		it "should be valid" do
				project.should be_valid
			end
		end
		
	  context "add a comment" do
		it "should have one comment after added" do
				project.comments<<comment
				project.comments.size.should eq 1
			end
		end
		
	  context "Project not done if a least one comment is open" do
		it "should not be done" do
				project.comments<<comment
				project.done?.should be_false
			end
		end

	  context "Project done if all its comments are closed" do
		it "should be done" do
				comment.workflow_state = 'closed'
				project.comments<<comment
				project.done?.should be_true 
			end
		end
		
	context "Project should return all its comments" do
		it "should retreve comments" do
				project.comments=[]
				project.items.to_a.should eq []
			end
		end

	context "project.recent_items should return the item created today" do
		it "should return an array with 1 item (comment)" do
				project.comments<<comment
				project.recent_items.size.should eq 1
			end
		end

	context "project.items_by_day should return items hashed by day" do
		it "should return an array with 1 key and one item as value" do
				project.comments<<comment
				x=project.items_by_day
				x.keys.size.should eq 1
				x.keys.first.should eq Time.now.to_date
				x[x.keys.first].size.should eq 1
				x[x.keys.first].first.should be_an_instance_of(Comment) 
			end
		end

   end
   
end

 