require 'spec_helper'

feature 'Creating Projects' do  
    scenario "Create a project"  do    
    visit '/'
    click_link 'New Project'
    fill_in 'Name', with: 'Project1'    
    click_button 'Create a Project'
    expect(page).to have_content('Project has been created.')  
    end 
end