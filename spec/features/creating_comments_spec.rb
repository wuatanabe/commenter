require 'spec_helper'

feature 'Creating Comments' do  
    scenario "Create a comment"  do    
    
    # To be Refactored 
    p = Project.new
    p.name = 'Project1'
    p.save!
    
    
    visit '/'

    click_link 'Project1'
    
    click_link 'New Comment'
    
    fill_in 'Body', with: 'Please, can I have a cup of tea now?' 
    click_button 'Create Comment'

    expect(page).to have_content('Comment has been created.')  
    end 
end