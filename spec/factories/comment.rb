

FactoryGirl. define do
	factory :comment do |f|
		f.body 'This is a fake comment'
		f.workflow_state 'open'
		f.project_id 1
	end
end