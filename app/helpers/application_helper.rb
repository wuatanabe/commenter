module ApplicationHelper


	def event_for(comment)
		return {:label => 'Close', :event => :close} if comment.current_state == 'open'
		return {:label => 'Repopen', :event => :reopen }
	end

	def replace_empty_with(e, s)
		return s if e.nil? or e.blank?
		return e
	end
	
	def label_for(done)
		 labels={:inprogress => "label label-default", :done => "label label-primary", :empty => "label label-default"}
		 return labels[:done] if done
		 return labels[:inprogress]
	end

	
	def 	project_details(p)
		return 'Empty' if p.comments.empty?
		return 'Done' if p.done?
		return 'In Progress'
	end
	
	def map_to_bootstrap_class(k)
	b= {:notice => "alert alert-warning", :error => "alert alert-danger"}
	return b[k.to_sym]
	end
	
end
