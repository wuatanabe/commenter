class CommentsController < ApplicationController
	
	
	before_action :load_project 
	
	def new
		@comments = comments.order("workflow_state ASC").order("created_at DESC")
	end
	
	#<%= render @project.comments %> 
	def create    
		@comment = comments.new(comment_params)    
		if @comment.save      
			respond_to do |format|        
				format.html { redirect_to @project, notice: 'Thanks for your comment' }        
				format.js      
			end    
		else      
			respond_to do |format|        
				format.html { redirect_to @project, alert: 'Unable to add comment' }        
			        format.js { render 'fail_create.js.erb' }      
			end   
		end  
	end
	
	def update
		c=Comment.find(params[:id])
		@comment= switch_state(c) 
		@link='#comment_'+@comment.id.to_s
		@project_status_link = @project.name+'_status'
		@project_status = @project.done?
		@comments = load_project.items
		@opencomments=   @comments.where('workflow_state = ?', 'open').order("created_at DESC") 
		@closedcomments= @comments.where('workflow_state = ?', 'closed').order("created_at DESC") 
		respond_to do |format|  
			if @comment.current_state == 'open'
					 format.js { render 'update_opencomments.js' }       
			else
					 format.js { render 'update_closedcomments.js' }       
			end
		end
	end

  private  
  
	def switch_state(c)
		(c.workflow_state == 'open')? c.update_attribute(:workflow_state, 'closed') : c.update_attribute(:workflow_state, 'open')
		c
	end
		
	def load_project    
		@project = Project.find(params[:project_id])  
	end  
	
	def comments
		@comments= @project.comments
	end
	
	def comment_params    
		params.require(:comment).permit(:body)  
	end
	
end
