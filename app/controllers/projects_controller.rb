class ProjectsController < ApplicationController
	
	def index
		@projects = Project.all
	end
	
	def new
		@project= Project.new
	end
	
	def show
		@comments = get_project.comments
		@opencomments= 	@comments.where('workflow_state = ?', 'open').order("created_at DESC") 
		@closedcomments= @comments.where('workflow_state = ?', 'closed').order("created_at DESC") 
	end
	
	def create
		@project = Project.new(project_params) 
		

		if @project.save    
			flash[:notice] = "Project has been created."    
			redirect_to :root
		else    
			 flash[:error]= "Error while saving #{@project.name}."
			 redirect_to :back
		end
		   
	end
	   
  def destroy
    get_project.destroy
    flash[:notice] = "Project #{@project.name} has been deleted."
    redirect_to :root
  end

private 

	def get_project    
		@project = Project.find(params[:id])  
	end  
  
  def project_params    
	  params.require(:project).permit(:name, :description)  
	  end
	
end
