class Project < ActiveRecord::Base
	has_many :comments, dependent: :destroy

	validates :name, uniqueness: true
	validates :name, uniqueness: true
	
	validates :name, presence: true
	
	def items
		comments
	end
	
	
	def done?
		items.where('workflow_state = ?', 'open').size == 0
	end
	
	
	def recent_items
		items.where('created_at > ?',  Date.yesterday.to_time.utc)
	end
	
	
	def items_by_day
		items.group_by &:created_on_day
	end
	
	
end
