class Comment < ActiveRecord::Base

	belongs_to :project

	validates :body, uniqueness: true
	
	validates :body, :project_id, presence: true
	
	
	include Workflow
	
	workflow do
		    state :open do
		      event :close, :transitions_to => :closed
		    end
	      
		    state :closed do
		      event :reopen, :transitions_to => :open
		    end
	end


	def created_on_day
		self.created_at.to_date
	end
	
end
